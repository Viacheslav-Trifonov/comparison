def compare(exp):
    if '<' in exp:
        x1 = int(exp.split('<')[0])
        x2 = int(exp.split('<')[1])
        return x1<x2
    else:
        x1 = int(exp.split('>')[0])
        x2 = int(exp.split('>')[1])
        return x1 > x2
